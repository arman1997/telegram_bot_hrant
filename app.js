const TelegramBot = require('node-telegram-bot-api');
const config = require('./config/config.json');
const token = config.bot_token;
const bot = new TelegramBot(token, {polling: true});

const log_module = require('./log')
const requests = require('./requests')

bot.onText(/\/start (.+)/, (msg, match) => {
    let reply = "";
    let chat_id = msg.chat.id;
    console.log(match)
    if(match[1] == null){
        reply = "Registration hash missing"
        bot.sendMessage(chat_id, reply);
        log_module.writeBotLog(msg, reply);
    } else {
        requests.check_user_hash(match[1],chat_id).then((data)=>{
            console.log(data, "data");
            if(data) {
                reply = "Welcome " + data.data.full_name;
                bot.sendMessage(chat_id, reply);
            } else {
                reply = "we cannot find ur account please contact administrator"
                bot.sendMessage(chat_id, reply);
            }
            log_module.writeBotLog(msg, reply);
        })
    }
});
    // bot.sendMessage(473026458, "nice")
    // bot.sendPhoto(473026458, 'https://www.racgp.org.au/getattachment/661bf5f7-d8fb-4457-b634-ae131183242a/Fellowship-programs-for-IMGs.aspx');
    async function send(chat_id, type = 'text', message){
        if(type == 'text'){
            return await bot.sendMessage(chat_id, message,  { parse_mode: "HTML" })
        } else if('image'){

        }
    }

    exports.send = send;