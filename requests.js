const axios = require('axios');
const config = require('./config/config.json');
const log_module = require('./log')

  check_user_hash =  (hash, chat_id) => new Promise((resolve, reject) => {
    let data = {key: config.service_api_key, hash: hash, chat_id: chat_id};
    let url = config.service_link + "/";
    let method = "post";
    axios.post(config.service_link + "/bot/validate_user", data)
    .then(function (response) {
      log_module.writeApiLog("success",url, method, JSON.stringify(data), response.data);
      console.log(response.data);
      if(response.data.status == '1'){
        resolve(response.data);
      } else {
        resolve(false)
      }
    })
    .catch(function (error) {
      if(error != null) {
        log_module.writeApiLog("error", error.config.url, error.config.method, error.config.data, JSON.stringify(error.response.data));
      }
    });
  });
  exports.check_user_hash = check_user_hash;