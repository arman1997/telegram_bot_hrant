const fs = require('fs');
const bot_log_dir = './logs_bot';
const api_log_dir = './logs_api';


function writeBotLog(msg, reply) {
    // let resp = match[1];
    let chat_id = msg.chat.id;
    let user_id = msg.from.id;
    let username = msg.from.username;
    let message_id = msg.message_id;
    let message = msg.text;
    fs.appendFile(
        bot_log_dir + '/' + date() + '.log',
        `**********\nchat_id: ${chat_id}\nuser_id: ${user_id}\nusername: ${username}\nmessage_id: ${message_id}\nmessage: "${message}"\nreply: "${reply}"\n`,
        function (err) {
            if (err) throw err;
        });
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function writeApiLog(status, link, method, data, resposnse) {
    let response = IsJsonString(resposnse) ? JSON.stringify(response) : resposnse;
    fs.appendFile(
        api_log_dir + '/' + date() + '.log',
        `*****${status}*****\nlink: ${link}\nmethod: ${method}\ndata: ${data}\nresposnse: ${resposnse}\n`,
        function (err) {
            if (err) throw err;
        });
}

function date() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    return dd + '_' + mm + '_' + yyyy;
}

exports.writeBotLog = writeBotLog;
exports.writeApiLog = writeApiLog;