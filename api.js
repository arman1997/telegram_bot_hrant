const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));

const port = 3000;
const bot = require('./app');


app.post('/send', (req, res) => { 
    let chat_id = req.body.chat_id;
    let message_type = req.body.message_type;
    let text_body = req.body.text;
    if(chat_id == null){
        res.send('no chat id');
        return;
    }
    if(message_type == "text"){
        bot.send(chat_id, message_type, text_body).then(data => {
            if(data['message_id'] != null){
                res.json({status: 'ok'});
            } else {
                res.json({status: 'not ok'});
            }
        }).catch(err => {
            res.json({status: 'not ok'});
        })
    }
})
app.post('/', (req, res) => {
    res.json({status: 1})
});

app.listen(port, () => {
    console.log(`Server is running. Port: ${port}`)
})